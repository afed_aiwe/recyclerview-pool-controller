package com.example.recyclerview_performance_extension

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview_performance_extension.poolcontroller.VhInitializer
import java.util.*

object GlobalRVPoolController {

    const val LOG_TAG = "RvPerfExtension"

    private var isInitialized = false
    private var mapOfStackVh = mutableMapOf<Int, Stack<RecyclerView.ViewHolder>>()
    private var mapOfMaxSizeCacheVh = mutableMapOf<Int, Int>()
    private lateinit var createVhDelegate: CreateVhDelegate

    fun initialize(params: List<VhCacheParams>, initializer: VhInitializer, createVhDelegate: CreateVhDelegate) {
        if (!checkDoubleViewType(params)) {
            isInitialized = true
            this.createVhDelegate = createVhDelegate
            initializer.initialize(params, createVhDelegate) { stack, maxCacheSize, viewType ->
                mapOfStackVh[viewType] = stack
                mapOfMaxSizeCacheVh[viewType] = maxCacheSize
            }
        } else {
            throw IllegalStateException("Duplicate ViewHolder type")
        }
    }

    fun getViewHolderForType(viewType: Int, isFromCreateViewHolder: Boolean = false): RecyclerView.ViewHolder? {
        require(isInitialized)
        return try {
            val stack = mapOfStackVh[viewType]
            stack?.pop()
        } catch (ex: EmptyStackException) {
            if (isFromCreateViewHolder) {
                createVhDelegate.createVhForTypeInBackground(viewType)
            } else {
                null
            }
        }
    }

    fun getRecycledViewCurrentCount(viewType: Int): Int {
        require(isInitialized)
        return mapOfStackVh[viewType]?.size ?: 0
    }

    fun putViewHolderToStack(viewType: Int, vh: RecyclerView.ViewHolder) {
        require(isInitialized)
        mapOfStackVh[viewType]?.push(vh)
    }

    private fun checkDoubleViewType(params: List<VhCacheParams>): Boolean {
        val mapOfViewType = mutableMapOf<Int, Boolean>()
        var result = false
        params.forEach {
            if (!mapOfViewType.containsKey(it.viewType)){
                mapOfViewType[it.viewType] = true
            } else {
                result = true
            }
        }
        return result
    }

    class VhCacheParams(val viewType: Int, val cacheSize: Int, val layoutId: Int? = null)

    interface CreateVhDelegate {

        fun createVhForTypeInBackground(viewType: Int): RecyclerView.ViewHolder? { return null }

        fun createVhForTypeWithAsyncInflater(viewType: Int, view: View): RecyclerView.ViewHolder? { return null }

    }

}