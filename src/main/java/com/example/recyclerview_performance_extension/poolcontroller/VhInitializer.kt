package com.example.recyclerview_performance_extension.poolcontroller

import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview_performance_extension.GlobalRVPoolController
import java.util.*

abstract class VhInitializer {

    abstract fun initialize(params: List<GlobalRVPoolController.VhCacheParams>,
                            createVhDelegate: GlobalRVPoolController.CreateVhDelegate,
                            callback: (Stack<RecyclerView.ViewHolder>, Int, Int) -> Unit)

}