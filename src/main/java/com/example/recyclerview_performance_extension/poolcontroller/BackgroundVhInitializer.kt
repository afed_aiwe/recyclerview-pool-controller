package com.example.recyclerview_performance_extension.poolcontroller

import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview_performance_extension.GlobalRVPoolController
import com.example.recyclerview_performance_extension.utils.VhUtils
import java.util.*
import java.util.concurrent.Executors

class BackgroundVhInitializer : VhInitializer() {

    private val executor = Executors.newFixedThreadPool(1)

    override fun initialize(params: List<GlobalRVPoolController.VhCacheParams>,
                            createVhDelegate: GlobalRVPoolController.CreateVhDelegate,
                            callback: (Stack<RecyclerView.ViewHolder>, Int, Int) -> Unit) {
        executor.execute {
            params.forEach { vhCacheParams ->
                val stack = Stack<RecyclerView.ViewHolder>()
                val viewType = vhCacheParams.viewType
                val maxCacheSize = vhCacheParams.cacheSize
                for (i in 1..maxCacheSize) {
                    val viewHolder = createVhDelegate.createVhForTypeInBackground(viewType)
                    viewHolder?.let { VhUtils.setItemViewTypeVh(it, viewType) }
                    stack.push(viewHolder)
                }
                callback(stack, maxCacheSize, viewType)
            }
        }
    }

}