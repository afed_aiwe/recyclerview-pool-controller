package com.example.recyclerview_performance_extension.poolcontroller

import android.content.Context
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview_performance_extension.GlobalRVPoolController
import com.example.recyclerview_performance_extension.utils.VhUtils
import java.util.*
import java.util.concurrent.Executors

class AsyncInflaterVhInitializer(val context: Context) : VhInitializer() {

    private var asyncLayoutInflater = AsyncLayoutInflater(context)

    private val executor = Executors.newFixedThreadPool(1)

    override fun initialize(params: List<GlobalRVPoolController.VhCacheParams>,
                            createVhDelegate: GlobalRVPoolController.CreateVhDelegate,
                            callback: (Stack<RecyclerView.ViewHolder>, Int, Int) -> Unit) {
        params.forEach {vhCacheParams ->
            val stack = Stack<RecyclerView.ViewHolder>()
            val viewType = vhCacheParams.viewType
            val layoutId = vhCacheParams.layoutId
            val maxCacheSize = vhCacheParams.cacheSize
            for (i in 1..maxCacheSize) {
                layoutId?.let { id ->
                    asyncLayoutInflater.inflate(id, null) {view, resid, parent ->
                        executor.execute {
                            val viewHolder = createVhDelegate.createVhForTypeWithAsyncInflater(viewType, view)
                            viewHolder?.let { VhUtils.setItemViewTypeVh(it, viewType) }
                            stack.push(viewHolder)
                        }
                    }
                }
            }
            callback(stack, maxCacheSize, viewType)
        }
    }
}