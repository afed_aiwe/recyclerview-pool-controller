package com.example.recyclerview_performance_extension

import androidx.recyclerview.widget.RecyclerView

class GlobalRVPool : RecyclerView.RecycledViewPool() {

    companion object {
        private const val LOG_TAG = "GlobalRecycledViewPool"
    }

    override fun getRecycledView(viewType: Int): RecyclerView.ViewHolder? {
        return GlobalRVPoolController.getViewHolderForType(viewType)
    }

    override fun getRecycledViewCount(viewType: Int): Int {
        val count = GlobalRVPoolController.getRecycledViewCurrentCount(viewType)
        return count
    }

    override fun putRecycledView(scrap: RecyclerView.ViewHolder?) {
        val viewType = scrap?.itemViewType ?: -1
        if (viewType != -1) {
            scrap?.let { GlobalRVPoolController.putViewHolderToStack(viewType, it) }
        }
    }
}